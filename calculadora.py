# 3rd party modules
from flask import make_response, abort


def sumar(num1, num2):
    return num1 + num2

def restar(num1, num2):
    return num1 - num2

def dividir(valores):
    print(valores)

    num1 = valores.get("num1")
    num2 = valores.get("num2")

    if num2 != 0:
        return num1/num2
    else:
        abort(404, "No se permite division entre 0")

def multiplicar(valores):
    print(valores)

    num1 = valores.get("num1")
    num2 = valores.get("num2")

    return num1 * num2